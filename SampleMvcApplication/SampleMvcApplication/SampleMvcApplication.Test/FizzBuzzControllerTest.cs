﻿
namespace SampleMvcApplication.Test
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using FizzBuzzDomain;
    using FizzBuzzLogic.Abstract;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using SampleMvcApplication.Controllers;
    using SampleMvcApplication.Models;

    [TestFixture]
    public class FizzBuzzControllerTest
    {
        private Mock<IFizzBuzz> mockFizzBuzz;

        private FizzBuzzController fizzBuzzController;

        [SetUp]
        public void SetUp()
        {
            this.mockFizzBuzz = new Mock<IFizzBuzz>();
            this.fizzBuzzController = new FizzBuzzController(this.mockFizzBuzz.Object);
        }

        [Test]
        public void WhenPageLoadThenExpectFizzBuzzViewWithNoOutput()
        {
            var result = this.fizzBuzzController.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            result.Should().BeOfType<ViewResult>();
            Assert.AreEqual(result.ViewName, "FizzBuzz");
            Assert.IsNotNull(result.Model);
            result.Model.Should().BeOfType<FizzBuzzModel>();
            Assert.IsFalse(((FizzBuzzModel)(result.Model)).OutPutData.Any());
        }

        [Test]
        public void WhenValidInputDataPassedThenExpectSuccess()
        {
            var vm = new FizzBuzzModel()
            {
                InputNumber = 5
            };

            var mockResult = new List<OutputData>()
            {
                new OutputData
                {
                    Output = "1"
                },
                    new OutputData
                {
                    Output = "2"
                },
                    new OutputData
                {
                    Output = "fizz"
                },
                    new OutputData
                {
                    Output = "4"
                },
                    new OutputData
                {
                    Output = "buzz"
                },
            };

            var domainData = new FizzBuzzData();
            domainData.OutPutData.AddRange(mockResult);
            this.mockFizzBuzz.Setup(x => x.Process(It.IsAny<int>())).Returns(domainData);
            var result = this.fizzBuzzController.ProcessFizzBuzz(vm) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            result.Should().BeOfType<ViewResult>();
            Assert.AreEqual(result.ViewName, "FizzBuzz");
            Assert.IsNotNull(result.Model);
            result.Model.Should().BeOfType<FizzBuzzModel>();
            Assert.IsTrue(((FizzBuzzModel)(result.Model)).OutPutData.Any());
        }

        [Test]
        public void WhenInValidInputDataPassedThenExpectValidationMessage()
        {
            var vm = new FizzBuzzModel()
            {
                InputNumber = 1200
            };

            this.fizzBuzzController.ModelState.AddModelError("InputNumber", "Please enter number between 1 to 1000");

            var result = this.fizzBuzzController.ProcessFizzBuzz(vm) as ViewResult;

            // Assert
             Assert.IsNotNull(result);
            result.Should().BeOfType<ViewResult>();
            Assert.AreEqual(result.ViewName, "FizzBuzz");
            Assert.IsNotNull(result.Model);
            result.Model.Should().BeOfType<FizzBuzzModel>();
            Assert.IsFalse(((FizzBuzzModel)(result.Model)).OutPutData.Any());
            Assert.IsFalse(this.fizzBuzzController.ModelState.IsValid);
        }
    }
}
