﻿
namespace FizzBuzzDomain
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class FizzBuzzData
    {
        public FizzBuzzData()
        {
            this.OutPutData = new List<OutputData>();
        }

        public List<OutputData> OutPutData { get; private set; }
    }
}
