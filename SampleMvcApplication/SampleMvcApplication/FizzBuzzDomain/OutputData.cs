﻿using System;

namespace FizzBuzzDomain
{
    public class OutputData
    {
        public string Output { get; set; }

        public string DisplayOutput
        {
            get
            {
                var result = this.Output.Replace("f", DateTime.Today.DayOfWeek.ToString().Substring(0, 1));
                result = result.Replace("b", DateTime.Today.DayOfWeek.ToString().Substring(0, 1));
                return result; 
            }
        }
    }
}
