﻿namespace SampleMvcApplication
{
    using System;
    using System.Web.Mvc;
    using System.Web.Routing;
    using StructureMap;

    // StructureMap ContollerFactory
    public class StructureMapControllerFactory : DefaultControllerFactory
    {
        protected override IController
            GetControllerInstance(RequestContext requestContext,
            Type controllerType)
        {
            try
            {
                if ((requestContext == null) || (controllerType == null))
                    return null;

                return (Controller)ObjectFactory.GetInstance(controllerType);
            }
            catch (StructureMapException)
            {
                throw new Exception("Couldn't able to initialize.");
            }
        }
    }
}