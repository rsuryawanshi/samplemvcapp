﻿namespace SampleMvcApplication
{
    using System.Web.Mvc;
    using FizzBuzzLogic.Abstract;
    using FizzBuzzLogic.Concrete;
    using StructureMap;

    public static class Bootstrapper
    {
        public static void Run()
        {
            ControllerBuilder.Current
                .SetControllerFactory(new StructureMapControllerFactory());

            ObjectFactory.Configure(x => x.For<IFizzBuzz>().Use<FizzBuzz>());
        }
    }
}