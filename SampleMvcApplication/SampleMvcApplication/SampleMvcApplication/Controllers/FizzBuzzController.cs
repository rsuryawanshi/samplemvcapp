﻿namespace SampleMvcApplication.Controllers
{
    using System.Web.Mvc;
    using AutoMapper;
    using FizzBuzzDomain;
    using FizzBuzzLogic.Abstract;
    using SampleMvcApplication.Models;

    public class FizzBuzzController : Controller
    {
        private IFizzBuzz fizzBuzz;

        public FizzBuzzController(IFizzBuzz fizzBuzz)
        {
            this.fizzBuzz = fizzBuzz;
        }

        //
        // GET: /FizzBuzz/

        [HttpGet]
        public ActionResult Index()
        {
            return this.View("FizzBuzz", new FizzBuzzModel());
        }

        [HttpPost]
        public ActionResult ProcessFizzBuzz(FizzBuzzModel viewModel)
        {
            if (ModelState.IsValid)
            {
                // below complete process can be done in separate file to make controller light weight.
                var fizzBuzzData = this.fizzBuzz.Process(viewModel.InputNumber);

                Mapper.CreateMap<OutputData, OutPutDataModel>();
                Mapper.CreateMap<FizzBuzzData, FizzBuzzModel>();
                var model = Mapper.Map<FizzBuzzData, FizzBuzzModel>(fizzBuzzData);
                // mapping can be done in separate file to make controller lightweight

                return this.View("FizzBuzz", model);
            }
            else
            {
                return this.View("FizzBuzz", viewModel);
            }
        }
    }
}
