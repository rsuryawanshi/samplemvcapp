﻿namespace FizzBuzzLogic.Concrete
{
    using System;
    using System.Globalization;
    using System.Linq;
    using FizzBuzzDomain;
    using FizzBuzzLogic.Abstract;

    public class FizzBuzz : IFizzBuzz
    {
        private const string PrintFizz = "fizz";
        private const string PrintBuzz = "buzz";
        private const string PrintFizzBuzz = "fizz buzz";

        public FizzBuzzData Process(int inputNumber)
        {
            var fizzbuzzData = new FizzBuzzData();
            fizzbuzzData.OutPutData.AddRange(Enumerable.Range(1, inputNumber).Select(GetOutPutData).ToList());
            return fizzbuzzData;
        }

        public OutputData GetOutPutData(int value)
        {
            var result = new OutputData();


            if (value % 15 == 0)
            {
                result.Output = PrintFizzBuzz;
            }
            else if (value % 5 == 0)
            {
                result.Output = PrintBuzz;
            }
            else
            {
                result.Output = value % 3 == 0 ? PrintFizz : value.ToString(CultureInfo.InvariantCulture);
            }

            return result;
        }
    }
}
