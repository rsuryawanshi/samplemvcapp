﻿namespace FizzBuzzLogic.Abstract
{
    using System.Collections.Generic;
    using FizzBuzzDomain;

    public interface IFizzBuzz
    {
        FizzBuzzData Process(int inputNumber);
    }
}
