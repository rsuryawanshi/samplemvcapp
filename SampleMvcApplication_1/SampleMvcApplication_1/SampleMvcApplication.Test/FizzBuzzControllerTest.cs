﻿
namespace SampleMvcApplication.Test
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using FizzBuzzDomain;
    using FizzBuzzLogic.Abstract;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using SampleMvcApplication.Commands.Abstract;
    using SampleMvcApplication.Controllers;
    using SampleMvcApplication.Models;

    [TestFixture]
    public class FizzBuzzControllerTest
    {
        private Mock<IServiceCommand<FizzBuzzModel, FizzBuzzModel>> mockFizzBuzz;

        private FizzBuzzController fizzBuzzController;

        [SetUp]
        public void SetUp()
        {
            this.mockFizzBuzz = new Mock<IServiceCommand<FizzBuzzModel, FizzBuzzModel>>();
            this.fizzBuzzController = new FizzBuzzController(this.mockFizzBuzz.Object);
        }

        [Test]
        public void WhenValidInputDataPassedThenExpectSuccess()
        {
            var vm = new FizzBuzzModel()
            {
                InputNumber = 5
            };

            var mockResult = new List<OutputData>()
            {
                new OutputData
                {
                    Output = "1"
                },
                    new OutputData
                {
                    Output = "2"
                },
                    new OutputData
                {
                    Output = "fizz"
                },
                    new OutputData
                {
                    Output = "4"
                },
                    new OutputData
                {
                    Output = "buzz"
                },
            };

            var domainData = new FizzBuzzData();
            domainData.OutPutData.AddRange(mockResult);
            this.mockFizzBuzz.Setup(x => x.Execute(It.IsAny<FizzBuzzModel>())).Returns(this.Output);
            var result = this.fizzBuzzController.ProcessFizzBuzz(vm) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            result.Should().BeOfType<ViewResult>();
            Assert.AreEqual(result.ViewName, "FizzBuzz");
            Assert.IsNotNull(result.Model);
            result.Model.Should().BeOfType<FizzBuzzModel>();
            Assert.IsTrue(((FizzBuzzModel)(result.Model)).OutPutData.Any());
        }

        [Test]
        public void WhenInValidInputDataPassedThenExpectValidationMessage()
        {
            var vm = new FizzBuzzModel()
            {
                InputNumber = 1200
            };

            this.fizzBuzzController.ModelState.AddModelError("InputNumber", "Please enter number between 1 to 1000");

            var result = this.fizzBuzzController.ProcessFizzBuzz(vm) as ViewResult;

            // Assert
             Assert.IsNotNull(result);
            result.Should().BeOfType<ViewResult>();
            Assert.AreEqual(result.ViewName, "FizzBuzz");
            Assert.IsNotNull(result.Model);
            result.Model.Should().BeOfType<FizzBuzzModel>();
            Assert.IsFalse(((FizzBuzzModel)(result.Model)).OutPutData.Any());
            Assert.IsFalse(this.fizzBuzzController.ModelState.IsValid);
        }

        private FizzBuzzModel Output()
        {
            var result = new FizzBuzzModel();
            var outputData = new List<OutPutDataModel>()
            {
                new OutPutDataModel()
                {
                    Output = "1"
                },
                    new OutPutDataModel
                {
                    Output = "2"
                },
                    new OutPutDataModel
                {
                    Output = "fizz"
                },
                    new OutPutDataModel
                {
                    Output = "4"
                },
                    new OutPutDataModel
                {
                    Output = "buzz"
                },
            };

            result.OutPutData.AddRange(outputData);
            return result;
        }
    }
}
