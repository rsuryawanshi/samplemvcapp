﻿namespace SampleMvcApplication.Test.Commands
{
    using System.Collections.Generic;
    using FizzBuzzDomain;
    using FizzBuzzLogic.Abstract;
    using Moq;
    using NUnit.Framework;
    using SampleMvcApplication.Commands.Concrete;
    using SampleMvcApplication.Models;

    public class GetOutputDataCommandTest
    {
        private Mock<IFizzBuzz> fizzBuzz;

        private GetOutputDataCommand getoutputCommand;

        [SetUp]
        public void SetUp()
        {
            this.fizzBuzz = new Mock<IFizzBuzz>();
            this.getoutputCommand = new GetOutputDataCommand(this.fizzBuzz.Object);
        }

        [Test]
        public void WhenValidDataPassedThenExpectSuccess()
        {
            this.fizzBuzz.Setup(x => x.Process(It.IsAny<int>())).Returns(this.Output);
            var model = new FizzBuzzModel
            {
                InputNumber = 5
            };

            var outputModel = this.getoutputCommand.Execute(model);

            // Assert
            Assert.IsNotNull(outputModel);
            Assert.IsNotNull(outputModel.OutPutData);
        }

        private FizzBuzzData Output()
        {
            var result = new FizzBuzzData();
            var outputData = new List<OutputData>()
            {
                new OutputData
                {
                    Output = "1"
                },
                    new OutputData
                {
                    Output = "2"
                },
                    new OutputData
                {
                    Output = "fizz"
                },
                    new OutputData
                {
                    Output = "4"
                },
                    new OutputData
                {
                    Output = "buzz"
                },
            };

            result.OutPutData.AddRange(outputData);
            return result;
        }
    }
}
