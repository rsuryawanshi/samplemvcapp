﻿namespace SampleMvcApplication.Models
{
    public class OutPutDataModel
    {
        public string Output { get; set; }
        public string DisplayOutput { get; set; }
    }
}