﻿namespace SampleMvcApplication.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [Serializable]
    public class FizzBuzzModel
    {
        public FizzBuzzModel()
        {
            this.OutPutData = new List<OutPutDataModel>();
        }

        [Display(Name = "Enter the number:")]
        [Range(1, 1000)]
        public int InputNumber { get; set; }

        public List<OutPutDataModel> OutPutData { get; set; }
    }
}