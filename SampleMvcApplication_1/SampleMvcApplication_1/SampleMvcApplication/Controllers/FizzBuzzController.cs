﻿namespace SampleMvcApplication.Controllers
{
    using System.Web.Mvc;
    using AutoMapper;
    using FizzBuzzDomain;
    using FizzBuzzLogic.Abstract;
    using SampleMvcApplication.Commands.Abstract;
    using SampleMvcApplication.Models;

    public class FizzBuzzController : Controller
    {
        private IServiceCommand<FizzBuzzModel, FizzBuzzModel> command;

        public FizzBuzzController(IServiceCommand<FizzBuzzModel, FizzBuzzModel> command)
        {
            this.command = command;
        }

        //
        // GET: /FizzBuzz/

        [HttpGet]
        public ActionResult ProcessFizzBuzz()
        {
            return this.View("FizzBuzz", new FizzBuzzModel());
        }

        [HttpPost]
        public ActionResult ProcessFizzBuzz(FizzBuzzModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = this.command.Execute(viewModel);
                return this.View("FizzBuzz", model);
            }
            else
            {
                return this.View("FizzBuzz", viewModel);
            }
        }
    }
}
