﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleMvcApplication.Commands.Concrete
{
    using AutoMapper;
    using FizzBuzzDomain;
    using FizzBuzzLogic.Abstract;
    using SampleMvcApplication.Commands.Abstract;
    using SampleMvcApplication.Models;

    public class GetOutputDataCommand : IServiceCommand<FizzBuzzModel, FizzBuzzModel>
    {
        public readonly IFizzBuzz fizzBuzz;

        public GetOutputDataCommand(IFizzBuzz fizzBuzz)
        {
            this.fizzBuzz = fizzBuzz;
        }

        public FizzBuzzModel Execute(FizzBuzzModel viewModel)
        {
            var fizzBuzzData = this.fizzBuzz.Process(viewModel.InputNumber);
            Mapper.CreateMap<OutputData, OutPutDataModel>();
            Mapper.CreateMap<FizzBuzzData, FizzBuzzModel>();
            return Mapper.Map<FizzBuzzData, FizzBuzzModel>(fizzBuzzData);
        }
    }
}