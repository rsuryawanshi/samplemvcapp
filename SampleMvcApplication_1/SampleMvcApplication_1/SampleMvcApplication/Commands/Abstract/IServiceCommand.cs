﻿
namespace SampleMvcApplication.Commands.Abstract
{
    public interface IServiceCommand<in TInput, out TModel>
    {
        TModel Execute(TInput viewModel);
    }
}
