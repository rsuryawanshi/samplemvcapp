﻿
namespace FizzBuzzLogic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using FizzBuzzDomain;
    using FizzBuzzLogic.Concrete;
    using NUnit.Framework;

    [TestFixture]
    public class FizzBuzzTest
    {
        private FizzBuzz fizzBuzz;

        [SetUp]
        public void SetUp()
        {
            this.fizzBuzz = new FizzBuzz();
        }

        [TestCase(3)]
        [TestCase(6)]
        [TestCase(9)]
        [TestCase(12)]
        [TestCase(18)]
        public void WhenNumberPassedOnlyDivisibleWith3ThenExpectFizz(int inputNumber)
        {
            var expectedResult = "fizz".Replace("f", DateTime.Today.DayOfWeek.ToString().Substring(0, 1));
            var outputString = this.fizzBuzz.GetOutPutData(inputNumber);

            // Assert
            Assert.IsFalse(string.IsNullOrEmpty(outputString.Output));
            Assert.AreEqual(outputString.Output, "fizz");
            Assert.AreEqual(outputString.DisplayOutput, expectedResult);
        }

        [TestCase(5)]
        [TestCase(10)]
        [TestCase(20)]
        [TestCase(25)]
        [TestCase(35)]
        public void WhenNumberPassedOnlyDivisibleWith5ThenExpectBuzz(int inputNumber)
        {
            var expectedResult = "buzz".Replace("b", DateTime.Today.DayOfWeek.ToString().Substring(0, 1));
            var outputString = this.fizzBuzz.GetOutPutData(inputNumber);

            // Assert
            Assert.IsFalse(string.IsNullOrEmpty(outputString.Output));
            Assert.AreEqual(outputString.Output, "buzz");
            Assert.AreEqual(outputString.DisplayOutput, expectedResult);

        }

        [TestCase(15)]
        [TestCase(30)]
        [TestCase(45)]
        [TestCase(60)]
        [TestCase(75)]
        public void WhenNumberPassedOnlyDivisibleWith3AND5ThenExpectFizzBuzz(int inputNumber)
        {
            var expectedResult = "fizz buzz".Replace("b", DateTime.Today.DayOfWeek.ToString().Substring(0, 1));
            expectedResult = expectedResult.Replace("f", DateTime.Today.DayOfWeek.ToString().Substring(0, 1));

            var outputString = this.fizzBuzz.GetOutPutData(inputNumber);

            // Assert
            Assert.IsFalse(string.IsNullOrEmpty(outputString.Output));
            Assert.AreEqual(outputString.DisplayOutput, expectedResult);
            Assert.AreEqual(outputString.Output, "fizz buzz");
        }

        [TestCase(16)]
        [TestCase(32)]
        [TestCase(2)]
        [TestCase(7)]
        [TestCase(8)]
        public void WhenNumberPassedNotDivisibleWith3AND5ThenExpectInput(int inputNumber)
        {
            var outputString = this.fizzBuzz.GetOutPutData(inputNumber);

            // Assert
            Assert.IsFalse(string.IsNullOrEmpty(outputString.Output));
            Assert.AreEqual(outputString.Output, inputNumber.ToString(CultureInfo.InvariantCulture));
        }

        [Test]
        public void When5NumberPassedThenActualResultAndExpectedResultMatch()
        {
            var mockResult = new List<OutputData>()
            {
                new OutputData
                {
                    Output = "1"
                },
                    new OutputData
                {
                    Output = "2"
                },
                    new OutputData
                {
                    Output = "fizz"
                },
                    new OutputData
                {
                    Output = "4"
                },
                    new OutputData
                {
                    Output = "buzz"
                },
            };
            var result = this.fizzBuzz.Process(5);

            // Assert
            var expectedResult = mockResult.Select(x => x.Output).ToList();
            var actualResult =  result.OutPutData.Select(x => x.Output).ToList();
            
            // Assert
            Assert.IsTrue(actualResult.SequenceEqual(expectedResult));
        }
    }
}
